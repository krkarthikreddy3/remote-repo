package com.retail.core;

public class Item {
	  long upc;
	  String description;
	  float price;
	  double weight;
	  String shipmethod;
	  double shippingCost;
	@Override
	public String toString() {
		return  upc + "\t" +description+ "\t"+ price + "\t"+weight+
				shipmethod +"\t"+shippingCost;
	}
	public double getShippingCost() {
		return shippingCost;
	}
	public void setShippingCost(double shippingCost) {
		this.shippingCost = shippingCost;
	}
	public Item (long upc, String description, float price, double weight, String shipmethod ) {
		this.upc = upc;
		this.description = description;
		this.price = price;
		this.weight = weight;
		this.shipmethod = shipmethod;
		
	}
	public Item() {
		
	}
	public long getUpc() {
		return upc;
	}
	public void setUpc(long upc) {
		this.upc = upc;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public String getShipmethod() {
		return shipmethod;
	}
	public void setShipmethod(String shipmethod) {
		this.shipmethod = shipmethod;
	}
	
}
