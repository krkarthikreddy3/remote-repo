package com.retail.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CostCalculation {
	
	public static void main(String[] args) {
		Item i =new Item();
		CaliculateAirShipCost c = new CaliculateAirShipCost();
		CalculateGroundCost g = new CalculateGroundCost();
	
		List<Item> report = new ArrayList<Item>();
		report.add(new Item(567321101987L, "CD � Pink Floyd, Dark Side Of The Moon" , 19.99f, 0.58 , "AIR"));
		report.add(new Item(567321101986L , "CD � Beatles, Abbey Road" , 17.99f, 0.61 , " GROUND"));
		report.add(new Item(567321101985L , "CD � Queen, A Night at the Opera" , 20.49f, 0.55 , "AIR"));
		report.add(new Item(567321101984L , "CD � Michael Jackson, Thriller" , 23.88f, 0.50 , " GROUND"));
		report.add(new Item(467321101899L , "iPhone - Waterproof Case" , 9.75f, 0.73 , "AIR"));
		report.add(new Item(477321101878L , "iPhone -  Headphones" , 17.25f, 3.21 , " GROUND")); 
		System.out.print("********Shipping report**********\t");
		System.out.println("\tDate:"+ new Date());
		System.out.println("UPC"+ "\t\t\t" +"Description"+"\t"+"Price"+"\t"+"Weight"+"\t"+"Ship Method"+"\t"+"Ship Cost"+"\n");
		double totalCost=0;
		for(Item items:report) {
			if(items.getShipmethod().equalsIgnoreCase("AIR")) {
				double airCost=c.airShippingCost(items);
				items.setShippingCost(airCost);
				System.out.println(items);
				totalCost+=airCost;
			}
			else {
				double groundCost=g.groundCost(items);
				items.setShippingCost(groundCost);
				System.out.println(items);
				totalCost+=groundCost;

			}
			
		}
		System.out.println("\nTotal Cost is = $"+totalCost);
		

	}
	
	
		
		
		

}
